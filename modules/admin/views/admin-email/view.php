<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AdminEmail */

$this->title = 'Эл. почта для связи';
$this->params['breadcrumbs'][] = ['label' => 'Эл. почта для связи', 'url' => ['index']];

\yii\web\YiiAsset::register($this);
?>
<div class="admin-email-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'email:email',
        ],
    ]) ?>

</div>
