<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TarifFeedback */

$this->title = 'Создание ';
$this->params['breadcrumbs'][] = ['label' => 'Заявки для тарифу', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarif-feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
