<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 25.02.2019
 * Time: 17:08
 */

namespace app\assets;

use yii\web\AssetBundle;


class publicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
        "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css",
        "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css",
        "https://unpkg.com/aos@next/dist/aos.css",
        "/public/css/cascade-slider.css",
        "/public/css/main.css",
    ];
    public $js = [
        "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
        "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js",
        "https://unpkg.com/aos@next/dist/aos.js",
        "/public/js/cascade-slider.js",
        "/public/js/cascade.js",
        "/public/js/main.js",
    ];
    public $depends = [

    ];
}
