<!-- rates-end -->
<footer id="footer" class="menu-event">
  <div class="line"></div>
  <div class="container footer d-flex justify-content-end">
    <div class="col-xl-5">
      <div class="footer-text" data-aos="fade-up">
        <h1>Остались вопросы?</h1>
        <p>Оставьте заявку и мы сразу ответим</p>
        <br>
        <button class="btn btn-primary btn-white" data-toggle="modal" data-target="#requestModal">Оставить заявку</button>
        <br>
        <br>
        <p>Либо звоните по телефону</p>
        <a href="tel:<?= Yii::$app->view->params['contact']->telephone; ?>">
          <h2><?= Yii::$app->view->params['contact']->telephone; ?></h2>
        </a>
        <br>
        <br>
        <img src="/public/images/header-logo.png" alt="">
      </div>
    </div>
  </div>
</footer>

