<!-- header -->
<header>
    <div class="col-xl-3 col-lg-2 p-0 d-flex h-100 align-items-center">
        <div class="logo-1 d-flex h-100 align-items-center mobile-btn">
            <img src="/public/images/header-img.png" alt="">
        </div>
        <div class="logo-2">
            <a href="#menu"><img src="/public/images/header-logo.png" alt=""></a>
        </div>
    </div>
    <div class="col-xl-5 col-lg-6 col-lg h-100">
        <div class="navigation h-100">
            <ul class="p-0 h-100 d-flex justify-content-between">
                
                <? foreach (Yii::$app->view->params['menu'] as $v):?>
                    <li class="<?=$class;?>">
                        <a href="<?=$v->url;?>"><?=$v->text;?></a>
                    </li>
                    <? $class = '';?>
                <? endforeach;?>
            </ul>
        </div>
    </div>
    <div class="col-xl-4 pr-4">
        <div class="call d-flex justify-content-end align-items-center">
            <a href="tel:<?=Yii::$app->view->params['contact']->telephone;?>"><h5><?=Yii::$app->view->params['contact']->telephone;?></h5></a>
            <button type="button" class="btn btn-outline-primary btn-snow" data-toggle="modal" data-target="#exampleModal">Задать вопрос</button>
        </div>
    </div>
</header>
<!-- header-end -->


<!-- header-mobile -->
<div class="navigation-mobile">
    <a href="#header"><img src="/public/images/header-logo.png" alt="" class="header-logo"></a>
    <ul class="mb-0">
        <!-- <? $class = 'active-link-mob';?> -->
        <? foreach (Yii::$app->view->params['menu'] as $v):?>
            <li class="<?=$class;?>">
                <? if($v->url == "#rates") $url = "#rates-mobile"; else $url = $v->url; ?>         
                <a href="<?=$url;?>"><?=$v->text;?></a>
            </li>
            <? $class = '';?>
        <? endforeach;?>
    </ul>
</div>
<!-- header-mobile -->