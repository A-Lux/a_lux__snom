<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute(['menu/index'])?>" class="logo">
            <span class="logo-mini"></span>
            <span class="logo-lg">Админ. Панель</span>
        </a>
<!--        --><?//= Html::a('<span class="logo-mini"></span><span class="logo-lg">Админ. панель</span>', ['default/index'], ['class' => 'logo']) ?>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"  title="Вопросы">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label <?=count(Yii::$app->view->params['feedback'])==0?'label-default':'label-success';?>"><?=count(Yii::$app->view->params['feedback']);?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">У вас <?=count(Yii::$app->view->params['feedback']);?> непрочитанных вопросы</li>
                            <li>

                                <ul class="menu">
                                    <? if(Yii::$app->view->params['feedback'] != null):?>
                                        <? foreach (Yii::$app->view->params['feedback'] as $v):?>
                                            <li>
                                                <a href="/admin/feedback/view?id=<?=$v->id;?>">
                                                    <i class="fa fa-user text-green"></i> <?=$v->fio;?>
                                                </a>
                                            </li>
                                        <? endforeach;?>
                                    <? endif;?>
                                </ul>
                            </li>
                            <li class="footer"><a href="/admin/feedback/">Посмотреть все вопросы</a></li>
                        </ul>
                    </li>
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Заявки">
                            <i class="fa fa-bell-o"></i>
                            <span class="label <?=count(Yii::$app->view->params['request'])==0?'label-default':'label-success';?>"><?=count(Yii::$app->view->params['request']);?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">У вас <?=count(Yii::$app->view->params['request']);?> непрочитанных заявки</li>
                            <li>
                                <ul class="menu">
                                    <? if(Yii::$app->view->params['request'] != null):?>
                                        <? foreach (Yii::$app->view->params['request'] as $v):?>
                                        <li>
                                            <a href="/admin/request/view?id=<?=$v->id;?>">
                                                <i class="fa fa-user text-green"></i> <?=$v->fio;?>
                                            </a>
                                        </li>
                                        <? endforeach;?>
                                    <? endif;?>

                                </ul>
                            </li>
                            <li class="footer"><a href="/admin/request/">Посмотреть все заявки</a></li>
                        </ul>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Заявки для тарифу">
                            <i class="fa fa-flag-o"></i>
                            <span class="label <?=count(Yii::$app->view->params['tarif'])==0?'label-default':'label-success';?>"><?=count(Yii::$app->view->params['tarif']);?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">У вас <?=count(Yii::$app->view->params['tarif']);?> заявка для тариф</li>
                            <li>
                                <ul class="menu">
                                    <? if(Yii::$app->view->params['tarif'] != null):?>
                                        <? foreach (Yii::$app->view->params['tarif'] as $v):?>
                                            <li>
                                                <a href="/admin/tarif-feedback/view?id=<?=$v->id;?>">
                                                    <i class="fa fa-user text-green"></i> <?=$v->fio;?>
                                                </a>
                                            </li>
                                        <? endforeach;?>
                                    <? endif;?>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="/admin/tarif-feedback">Посмотреть все заявки для тариф</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Админ">
                            <!-- The user image in the navbar-->
                            <img src="/private/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">Alexander Pierce</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="/private/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    Alexander Pierce - Administrator
                                    <small>2017-2019</small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="/admin/admin-profile/index" class="btn btn-default btn-flat">Профиль</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/auth/logout" class="btn btn-default btn-flat">Выйти</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/private/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>Alexander Pierce</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Поиск ...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">ОСНОВНАЯ НАВИГАЦИЯ</li>
            </ul>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [

                        ['label' => 'Меню', 'icon' => 'fa fa-user', 'url' => ['/admin/menu/'],'active' => $this->context->id == 'menu'],
                        ['label' => 'Баннер', 'icon' => 'fa fa-user', 'url' => ['/admin/banner/'],'active' => $this->context->id == 'banner'],
                        ['label' => 'Заголовок блока', 'icon' => 'fa fa-user', 'url' => ['/admin/title/'],'active' => $this->context->id == 'title'],
                        ['label' => 'Что такое VOIP?', 'icon' => 'fa fa-user', 'url' => ['/admin/voip/'],'active' => $this->context->id == 'voip'],
                        ['label' => 'Почему именно IP ', 'icon' => 'fa fa-user', 'url' => ['/admin/about/'],'active' => $this->context->id == 'about'],
                        ['label' => 'Преимущества', 'icon' => 'fa fa-user', 'url' => ['/admin/advantage/'],'active' => $this->context->id == 'advantage'],
                        ['label' => 'Тарифы', 'icon' => 'fa fa-user', 'url' => ['/admin/tarif/'],'active' => $this->context->id == 'tarif'],
                        [
                            'label' => 'Контакты',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/admin/contact/'],'active' => $this->context->id == 'contact'],
                                ['label' => 'Эл. почта для связи', 'icon' => 'fa fa-user', 'url' => ['/admin/admin-email/'],'active' => $this->context->id == 'admin-email'],
                            ],
                        ],
//                        ['label' => 'Заявки', 'icon' => 'fa fa-user', 'url' => ['/admin/request/'],'active' => $this->context->id == 'request'],
//                        ['label' => 'Вопросы', 'icon' => 'fa fa-user', 'url' => ['/admin/feedback/'],'active' => $this->context->id == 'feedback'],

                    ],
                ]
            ) ?>
        </section>
    </aside>



    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?=$content?>

        </section>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
        reserved.
    </footer>


    <div class="control-sidebar-bg"></div>
</div>

<?php $this->endBody() ?>
<?php $this->registerJsFile('/ckeditor/ckeditor.js');?>
<?php $this->registerJsFile('/ckfinder/ckfinder.js');?>
<script>
    $(document).ready(function(){
        var editor = CKEDITOR.replaceAll();
        CKFinder.setupCKEditor( editor );
    });
    $.widget.bridge('uibutton', $.ui.button);
</script>
</body>
</html>
<?php $this->endPage() ?>
