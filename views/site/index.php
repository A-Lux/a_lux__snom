<!--main-slider  -->
<div class="main-slider menu-event" id="menu">
    <? foreach ($banner as $v) : ?>
        <div class="slider-content">
            <img src="/public/images/slider-1.png" alt="">
            <div class="slider-text">
                <h1><?= $v->name; ?></h1>
                <p><?= $v->content; ?></p>
                <button type="button" class="btn btn-primary btn-snow" data-toggle="modal" data-target="#bannerModal">Подобрать тариф</button>
            </div>
            <div class="slider-item">
                <div class="phone">
                    <div class="ip-phone">
                        <p>IP-телефон высшего класса <span>Snom D785</span></p>
                        <button class="btn btn-link" data-toggle="modal" data-target="#bannerModal">ХОЧУ ТАКОЙ ЖЕ</button>
                    </div>
                    <img src="<?= $v->getImage() ?>" alt="" data-aos="fade-left">
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>

<!--main-slider-end  -->

<!-- voip -->
<div class="voip menu-event" id="main" data-aos="fade-right">
    <div class="line"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-md-5 text-center">
                <img src="<?= $voip->getImage() ?>" alt="">
            </div>
            <div class="col-xl-7 col-md-7">
                <div class="voip-text">
                    <br>
                    <div class="title">
                        <h1><?= $title[0]->name; ?></h1>
                    </div>
                    <br>
                    <?= $voip->content; ?>
                    <br>
                    <button class="btn btn-primary btn-white" data-toggle="modal" data-target="#tarifModal">Подробнее</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- voip-end -->

<!-- Ip=telefonia -->
<div class="telefonia menu-event" id="telefonia">
    <div class="line"></div>
    <div class="container">
        <div class="title">
            <h1><?= $title[1]->name; ?></h1>
        </div>
        <br>
        <br>
        <? $m = 0; ?>
        <? foreach ($about as $v) : ?>
            <? $m++; ?>
            <? if ($m % 5 == 1) : ?>
                <div class="tel-block">
                <? endif; ?>
                <div class="tel-content text-center">
                    <img src="<?= $v->getImage(); ?>" alt="" class="advan-img" data-aos="zoom-in-up">
                    <p><?= $v->text; ?></p>
                </div>
                <? if ($m % 5 == 0) : ?>
                </div>
                <br>
            <? endif; ?>
        <? endforeach; ?>

        <? if ($m % 5 != 0) : ?>
    </div>
    <br>
<? endif; ?>

</div>
</div>

<!-- Ip=telefonia-end -->

<!-- advantages -->
<div class="advantages menu-event" id="benefits">
    <div class="line"></div>
    <div class="container">
        <div class="title" data-aos="zoom-in">
            <h1><?= $title[2]->name; ?></h1>
        </div>
        <br><br>
        <div class="row">
            <? foreach ($advantages as $v) : ?>
                <div class="col-xl-4">
                    <div class="adventages-content">
                        <img src="<?= $v->getImage(); ?>" alt="" data-aos="zoom-in">
                        <div class="adventages-text">
                            <p class="text-bold"><?= $v->text_top; ?></p>
                            <p><?= $v->text_bottom; ?></p>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>

<!-- advantages-end -->

<!-- rates -->
<div class="rates menu-event" id="rates">
    <div class="line"></div>
    <div class="container" data-aos="zoom-in">
        <div class="title">
            <h1><?= $title[3]->name; ?></h1>
        </div>
        <br><br>

        <div class="cascade-slider_container" id="cascade-slider">
            <div class="cascade-slider_slides">
                <? foreach ($tarif as $v) : ?>
                    <div class="cascade-slider_item" data-slider-id="<?= $v->id; ?>">
                        <div class="slider-header text-center">
                            <img src="<?= $v->getImage(); ?>" alt="">
                            <h1><?= $v->name; ?></h1>
                            <p><?= $v->subname; ?></p>
                        </div>
                        <div class="slider-body">
                            <?= $v->top_content; ?>

                            <div class="row">
                                <div class="col-xl-6 col-md-6">
                                    <?= $v->left_content; ?>
                                </div>
                                <div class="col-xl-6 col-md-6 p-0">
                                    <div class="slider-left-block text-center">
                                        <?= $v->right_content; ?>
                                        <br>
                                        <button class="btn btn-primary btn-white" data-toggle="modal" data-target="#tarifModal">Подобрать тариф</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>

            <span class="cascade-slider_arrow cascade-slider_arrow-left" data-action="prev"><img src="/public/images/prev-canvas.png" alt=""></span>
            <span class="cascade-slider_arrow cascade-slider_arrow-right" data-action="next"><img src="/public/images/next-canvas.png" alt=""></span>
        </div>

    </div>
</div>


<!-- rates-slide-mobile -->
<div class="rates-mobile" id="rates-mobile">
    <div class="container">
        <div class="title">
            <h1><?= $title[3]->name; ?></h1>
        </div>
    </div>
</div>
<div class="rates-slider-mobile menu-event">
    <? foreach ($tarif as $v) : ?>
        <div class="slider-content" data-aos="fade-down">
            <div class="slider-header text-center">
                <img src="<?= $v->getImage(); ?>" alt="">
                <h1><?= $v->name; ?></h1>
                <p><?= $v->subname; ?></p>
            </div>
            <div class="slider-body">
                <?= $v->top_content; ?>
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <?= $v->left_content; ?>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="slider-left-block text-center">
                            <?= $v->right_content; ?>
                            <br>
                            <button class="btn btn-primary btn-white" data-toggle="modal" data-target="#tarifModal">Подобрать тариф</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>

<!-- rates-slide-mobile-end -->

<!-- modal -->


<!-- modal-end -->