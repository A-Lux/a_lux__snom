function showSuccessAlert(text){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true
    });
    Toast.fire({
        icon: 'success',
        title: text
    });
}


$('input[name="Feedback[telephone]"]').inputmask("8(999) 999-9999");

$('input[name="Feedback[fio]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я]+$/;
    if (regex.test(this.value) !== true) {
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
    }
});



$('input[name="Request[telephone]"]').inputmask("8(999) 999-9999");

$('input[name="Request[fio]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я]+$/;
    if (regex.test(this.value) !== true) {
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
    }
});



$('input[name="TarifFeedback[telephone]"]').inputmask("8(999) 999-9999");

$('input[name="TarifFeedback[fio]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я]+$/;
    if (regex.test(this.value) !== true) {
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
    }
});




$('body').on('click','#sendFeedback', function(){
    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/site/feedback',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                Swal.close();
                Swal.fire({
                    icon: 'success',
                    title: 'Заявка ушпешно отправлен!',
                    text: 'В ближайщее время мы свяжемся с вами.',
                });
                $('input[name="Feedback[telephone]"]').val('');
                $('input[name="Feedback[fio]"]').val('');
                $('input[name="Feedback[email]"]').val('');
                $('textarea[name="Feedback[content]"]').val('');
            }else{
                Swal.close();
                Swal.fire({
                    icon: 'error',
                    text: response,
                });

            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Что-то пошло не так!',
            });
        }
    });
});




$('body').on('click','#sendRequest', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/site/request',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                Swal.close();
                Swal.fire({
                    icon: 'success',
                    title: 'Заявка ушпешно отправлен!',
                    text: 'В ближайщее время мы свяжемся с вами.',
                });
                $('input[name="Request[fio]"]').val('');
                $('input[name="Request[telephone]"]').val('');
                $('input[name="Request[email]"]').val('');
            }else{
                Swal.close();
                Swal.fire({
                    icon: 'error',
                    text: response,
                });

            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Что-то пошло не так!',
            });
        }
    });
});




$('body').on('click','#sendTarif', function(){

    Swal.showLoading();
    var tarif = $('.now').attr('data-slider-id');
    $('#tarifModal .modal-body').append('<input type="hidden"  name="TarifFeedback[tarif_id]" value="'+tarif+'">');
    $.ajax({
        type: 'POST',
        url: '/site/tarif',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                $('input[name="TarifFeedback[tarif_id]"]').remove();
                Swal.close();
                Swal.fire({
                    icon: 'success',
                    title: 'Заявка ушпешно отправлен!',
                    text: 'В ближайщее время мы свяжемся с вами.',
                });
                $('input[name="TarifFeedback[fio]"]').val('');
                $('input[name="TarifFeedback[telephone]"]').val('');
                $('input[name="TarifFeedback[email]"]').val('');

            }else{
                $('input[name="TarifFeedback[tarif_id]"]').remove();
                Swal.close();
                Swal.fire({
                    icon: 'error',
                    text: response,
                });
            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Что-то пошло не так!',
            });
        }
    });
});





$('body').on('click','#sendBanner', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/site/banner',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                Swal.close();
                Swal.fire({
                    icon: 'success',
                    title: 'Заявка ушпешно отправлен!',
                    text: 'В ближайщее время мы свяжемся с вами.',
                });
                $('input[name="Request[fio]"]').val('');
                $('input[name="Request[telephone]"]').val('');
                $('input[name="Request[email]"]').val('');
            }else{
                Swal.close();
                Swal.fire({
                    icon: 'error',
                    text: response,
                });

            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Что-то пошло не так!',
            });
        }
    });
});
