const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');


module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'main.js',
    path: path.join(__dirname, './public/js')
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpg|gif|svg|otf)$/i,
        use: [
          {
            loader: 'url-loader'
          },
        ],
      }
    ],
    
  },
  optimization: {
    minimizer: [new UglifyJsPlugin()],
  }
};