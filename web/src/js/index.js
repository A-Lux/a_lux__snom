window.$ = require("jquery");
require('slick-carousel');

console.log('hello-world');
$(document).ready(function () {
    AOS.init();

    $('.main-slider').slick({
        dots: false,
        autoplay: false,
        autoplaySpeed: 3000,
        arrows: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
    });

    $('.rates-slider-mobile').slick({
        dots: true,
        autoplay: false,
        autoplaySpeed: 3000,
        arrows: true,
        infinite: true,
        speed: 500,
    });


    $(window).on("scroll", function () {
        if ($(window).scrollTop() > 500) {
            $("header").addClass("active");
        } else {
            $("header").removeClass("active");
        }
    });

    $('header a').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top }, 1000);
    });

    $('.navigation li').click(function () {
        $('.navigation li').removeClass('active-link');
        $(this).addClass('active-link');
    });

    $(window).scroll(function(){
        
        $('.menu-event').each(function(){

            var idDiv = $(this).attr('id');

            if(
                ($(window).scrollTop() > $(this).offset().top - 510) &&
                ($(window).scrollTop() < ($(this).offset().top - 600 + $(this).outerHeight()))
            ){
                $('header .navigation a[href="#'+ idDiv +'"]').parent().addClass('active-link');
            } else {
                $('header .navigation a[href="#'+ idDiv +'"]').parent().removeClass('active-link');
            }
        });

        $('.menu-event').each(function(){

            var idDiv = $(this).attr('id');

            if(
                ($(window).scrollTop() > $(this).offset().top - 400) &&
                ($(window).scrollTop() < ($(this).offset().top - 600 + $(this).outerHeight()))
            ){
                $('.navigation-mobile ul li a[href="#'+ idDiv +'"]').parent().addClass('active-link-mob');
            } else {
                $('.navigation-mobile ul li a[href="#'+ idDiv +'"]').parent().removeClass('active-link-mob');
            }
        });
    })


    // mobile-nav
    $('.navigation-mobile a').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top }, 1000);
    });
    $('.navigation-mobile li').click(function () {
        $('.navigation-mobile li').removeClass('active-link-mob');
        $(this).addClass('active-link-mob');
    });
    // mobile-nav
});


