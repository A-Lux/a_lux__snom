<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.04.2019
 * Time: 11:45
 */

namespace app\controllers;



use app\models\AdminEmail;
use app\models\Contact;
use app\models\Menu;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{

    public function init()
    {

        $menu = Menu::getAll();
        $contact = Contact::find()->one();

        Yii::$app->view->params['menu'] = $menu;
        Yii::$app->view->params['contact'] = $contact;
    }



    protected function setMeta($title = null, $keywords = null, $description = null){
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
    }





}