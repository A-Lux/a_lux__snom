<?php

namespace app\controllers;


use app\models\About;
use app\models\AdminEmail;
use app\models\Advantage;
use app\models\Banner;
use app\models\Feedback;
use app\models\Menu;


use app\models\Request;
use app\models\Tarif;
use app\models\TarifFeedback;
use app\models\Title;
use app\models\Voip;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class SiteController extends FrontendController
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'foreColor' => 0xffffff,
                'offset' => 3,
                'backColor' =>  0x4a5e68,
                'fontFile'  => '@app/web/fonts/Century Gothic.ttf',
            ],
        ];
    }


    public function actionIndex()
    {

        $this->setMeta('Snom');
        $title = Title::getAll();
        $banner = Banner::getAll();
        $voip = Voip::getData();
        $about = About::getAll();
        $advantages = Advantage::getAll();
        $tarif = Tarif::getAll();

        return $this->render('index',compact('banner','voip','about','advantages','tarif','title'));
    }







    public function actionFeedback()
    {
        $model = new Feedback();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $phoneStatus = true;
            for ($i = 0; $i < strlen($model->telephone); $i++) {
                if (($model->telephone)[$i] == '_') {
                    $phoneStatus = false;
                }
            }
            if($phoneStatus){
                if($model->save()) $this->sendFeedback($model->fio, $model->telephone, $model->email, $model->content);
                return 1;
            }else{
                return "Необходимо заполнить «Телефон».";
            }

        }else{
            $error = "";
            $errors = $model->getErrors();
            foreach($errors as $v){
                $error .= $v[0];
                break;
            }
            return $error;
        }

    }




    public function actionRequest()
    {
        $model = new Request();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $phoneStatus = true;
            for ($i = 0; $i < strlen($model->telephone); $i++) {
                if (($model->telephone)[$i] == '_') {
                    $phoneStatus = false;
                }
            }
            if($phoneStatus){
                if($model->save()) $this->sendRequest($model->fio, $model->telephone, $model->email,"Клиент хочет связаться с вами");
                return 1;
            }else{
                return "Необходимо заполнить «Телефон».";
            }

        }else{
            $error = "";
            $errors = $model->getErrors();
            foreach($errors as $v){
                $error .= $v[0];
                break;
            }
            return $error;
        }
    }




    public function actionTarif()
    {
        $model = new TarifFeedback();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $phoneStatus = true;
            for ($i = 0; $i < strlen($model->telephone); $i++) {
                if (($model->telephone)[$i] == '_') {
                    $phoneStatus = false;
                }
            }
            if($phoneStatus){
                if($model->save()) $this->sendTarif($model->fio, $model->telephone, $model->email, $model->tarif->name);
                return 1;
            }else{
                return "Необходимо заполнить «Телефон».";
            }

        }else{
            $error = "";
            $errors = $model->getErrors();
            foreach($errors as $v){
                $error .= $v[0];
                break;
            }
            return $error;
        }
    }





    public function actionBanner()
    {
        $model = new Request();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $phoneStatus = true;
            for ($i = 0; $i < strlen($model->telephone); $i++) {
                if (($model->telephone)[$i] == '_') {
                    $phoneStatus = false;
                }
            }
            if($phoneStatus){
                if($model->save()) $this->sendRequest($model->fio, $model->telephone, $model->email, "Клиент хочет связаться с вами (Баннер).");
                return 1;
            }else{
                return "Необходимо заполнить «Телефон».";
            }

        }else{
            $error = "";
            $errors = $model->getErrors();
            foreach($errors as $v){
                $error .= $v[0];
                break;
            }
            return $error;
        }
    }



    private function sendFeedback($name,$phone, $email, $content) {

        $admin_email = AdminEmail::getAdminEmail();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
            ->setTo($admin_email)
            ->setSubject('Клиент задал вопрос')
            ->setHtmlBody("<p>ФИО: $name</p>
                                 <p>Email: $email</p>
                                 <p>Телефон: $phone</p>
                                 <p>Вопрос: $content</p>");
        return $emailSend->send();
    }



    private function sendRequest($name,$phone, $email, $subject) {

        $admin_email = AdminEmail::getAdminEmail();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
            ->setTo($admin_email)
            ->setSubject($subject)
            ->setHtmlBody("<p>ФИО: $name</p>
                                 <p>Email: $email</p>
                                 <p>Телефон: $phone</p>");
        return $emailSend->send();
    }



    private function sendTarif($name,$phone, $email, $tarif) {

        $admin_email = AdminEmail::getAdminEmail();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
            ->setTo($admin_email)
            ->setSubject('Клиент хочет связаться с вами по подбору тарифа')
            ->setHtmlBody("<p>ФИО: $name</p>
                                 <p>Email: $email</p>
                                 <p>Телефон: $phone</p>
                                 <p>Тариф: $tarif</p>");
        return $emailSend->send();
    }


}
