<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin_email".
 *
 * @property int $id
 * @property string $email
 */
class AdminEmail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
        ];
    }

    public static function getAdminEmail(){
        $admin = AdminEmail::find()->one();
        return $admin->email;
    }
}
