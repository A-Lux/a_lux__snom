<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property string $fio
 * @property string $telephone
 * @property string $email
 * @property int $isRead
 * @property string $created
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'telephone', 'email'], 'required'],
            [['email'],'email'],
            [['isRead'], 'integer'],
            [['created'], 'safe'],
            [['fio', 'telephone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Фио',
            'telephone' => 'Телефон',
            'email' => 'E-mail',
            'isRead' => 'Прочитано',
            'created' => 'Дата создание',
        ];
    }

    public function saveRequest($name,$phone, $email){
        $this->fio = $name;
        $this->telephone = $phone;
        $this->email = $email;
        $this->isRead = 0;
        if($this->save(false)){
            $array = ['status' => 1];
        }else{
            $array = ['status' => 0];
        }
        return $array;
    }
}
