<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advantage".
 *
 * @property int $id
 * @property string $text_top
 * @property string $text_bottom
 * @property string $image
 */
class Advantage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advantage';
    }

    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/advantages/';
    public function rules()
    {
        return [
            [['text_top', 'text_bottom'], 'required'],
            [['text_top', 'text_bottom'], 'string'],
            [['image'], 'file', 'extensions' => 'png,jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_top' => 'Текст вверху',
            'text_bottom' => 'Текст снизу',
            'image' => 'Картинка',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/' . $this->path . $this->image : '/no-image.png';
    }

    public static function getAll(){
        return Advantage::find()->all();
    }
}
