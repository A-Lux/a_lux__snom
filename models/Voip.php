<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "voip".
 *
 * @property int $id
 * @property string $content
 * @property string $image
 */
class Voip extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/voip/';
    public static function tableName()
    {
        return 'voip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], 'string'],
            [['image'], 'file', 'extensions' => 'png,jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Содержание',
            'image' => 'Картинка',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/' . $this->path . $this->image : '/no-image.png';
    }

    public static function getData(){
        return Voip::find()->one();
    }
}
