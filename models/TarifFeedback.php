<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tarif_feedback".
 *
 * @property int $id
 * @property string $fio
 * @property string $telephone
 * @property string $email
 * @property int $tarif_id
 * @property int $isRead
 * @property string $created
 */
class TarifFeedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tarif_feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'telephone', 'email', 'tarif_id'], 'required'],
            [['tarif_id', 'isRead'], 'integer'],
            [['created'], 'safe'],
            [['email'],'email'],
            [['fio', 'telephone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Фио',
            'telephone' => 'Телефон',
            'email' => 'E-mail',
            'tarif_id' => 'Тариф',
            'isRead' => 'Прочитано',
            'created' => 'Дата создание',
        ];
    }

    public function saveRequest($name,$phone, $email, $tarif_id){
        $this->fio = $name;
        $this->telephone = $phone;
        $this->email = $email;
        $this->tarif_id = $tarif_id;
        $this->isRead = 0;
        if($this->save(false)){
            $array = ['status' => 1];
        }else{
            $array = ['status' => 0];
        }
        return $array;
    }

    public function getTarif()
    {
        return $this->hasOne(Tarif::className(), ['id' => 'tarif_id']);
    }
}
