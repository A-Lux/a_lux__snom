<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tarif".
 *
 * @property int $id
 * @property string $name
 * @property string $subname
 * @property string $image
 * @property string $top_content
 * @property string $left_content
 * @property string $right_content
 */
class Tarif extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/tarif/';
    public static function tableName()
    {
        return 'tarif';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'subname', 'top_content', 'left_content', 'right_content'], 'required'],
            [['top_content', 'left_content', 'right_content'], 'string'],
            [['name', 'subname'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'subname' => 'Пакет',
            'image' => 'Картинка',
            'top_content' => 'Содержание вверху',
            'left_content' => 'Содержание слева',
            'right_content' => 'Содержание справа',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/' . $this->path . $this->image : '/no-image.png';
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Tarif::find()->all(),'id','name');
    }

    public static function getAll(){
        return Tarif::find()->all();
    }
}
